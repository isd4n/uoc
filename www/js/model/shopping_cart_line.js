/* globals angular */
(function (angular) {
    'use strict';

    var ShoppingCartLine = function ShoppingCartLine(jsonData) {

            if (jsonData === undefined || jsonData === null) {
                return;
            }

            this.quantity = jsonData.quantity;

            this.item = jsonData.item;

            this.price = jsonData.price;
        },

        module = angular.module('starter');

    module.factory('ShoppingCartLine', function () {
        return ShoppingCartLine;
    });

}(angular));
