/* globals angular */
(function (angular) {
    'use strict';

    var Book = function Book(jsonData) {

        if (jsonData === undefined || jsonData === null) {
            return;
        }

        this.id = jsonData.id;

        this.title = jsonData.title;

        this.cover = jsonData.cover;

        this.author = jsonData.author;

        this.price = parseInt(jsonData.price);

        this.review = jsonData.review;

        this.ISBN = jsonData.ISBN;

    },

    module = angular.module('starter');

    module.factory('Book', function () {
        return Book;
    });

}(angular));
