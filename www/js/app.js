(function () {
  angular.module('starter', ['ionic', 'ngMessages'])

    .run(function($ionicPlatform) {
      $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
          cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
          // org.apache.cordova.statusbar required

            StatusBar.styleLightContent();
        }
      });
    })

    .config(function($stateProvider, $urlRouterProvider) {
      // Ionic uses AngularUI Router which uses the concept of states
      // Learn more here: https://github.com/angular-ui/ui-router
      // Set up the various states which the app can be in.
      // Each state's controller can be found in controllers.js
      $stateProvider

      // setup an abstract state for the tabs directive
          .state('main', {
              url: '/',
              templateUrl: 'templates/main.html',
              controller: 'MainController'
          })

          .state('login', {
            url: '/login',
            templateUrl: 'templates/login.html',
            controller: 'LoginController'
          })

          .state('books', {
            url: '/books',
            templateUrl: 'templates/book-list.html',
            controller: 'BookListController'
          })

          .state('book-detail', {
            url: '/books/:bookId',
            templateUrl: 'templates/book-detail.html',
            controller: 'BookDetailController'
          })

          .state('cart', {
              url: '/cart',
              templateUrl: 'templates/cart.html',
              controller: 'CartController'
          })

      ;

      // if none of the above states are matched, use this as the fallback
      $urlRouterProvider.otherwise('login');

    });
}());

