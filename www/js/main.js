(function () {
  angular.module('starter', ['ionic', 'ngMessages'])

    .run(function($ionicPlatform) {
      $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
          cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
          // org.apache.cordova.statusbar required

            StatusBar.styleLightContent();
        }
      });
    })

    .config(function($stateProvider, $urlRouterProvider) {
      // Ionic uses AngularUI Router which uses the concept of states
      // Learn more here: https://github.com/angular-ui/ui-router
      // Set up the various states which the app can be in.
      // Each state's controller can be found in controllers.js
      $stateProvider

      // setup an abstract state for the tabs directive
          .state('main', {
              url: '/',
              templateUrl: 'templates/main.html',
              controller: 'MainController'
          })

          .state('login', {
            url: '/login',
            templateUrl: 'templates/login.html',
            controller: 'LoginController'
          })

          .state('books', {
            url: '/books',
            templateUrl: 'templates/book-list.html',
            controller: 'BookListController'
          })

          .state('book-detail', {
            url: '/books/:bookId',
            templateUrl: 'templates/book-detail.html',
            controller: 'BookDetailController'
          })

          .state('cart', {
              url: '/cart',
              templateUrl: 'templates/cart.html',
              controller: 'CartController'
          })

      ;

      // if none of the above states are matched, use this as the fallback
      $urlRouterProvider.otherwise('login');

    });
}());


/* globals angular */
(function (angular) {
    'use strict';

    var Book = function Book(jsonData) {

        if (jsonData === undefined || jsonData === null) {
            return;
        }

        this.id = jsonData.id;

        this.title = jsonData.title;

        this.cover = jsonData.cover;

        this.author = jsonData.author;

        this.price = parseInt(jsonData.price);

        this.review = jsonData.review;

        this.ISBN = jsonData.ISBN;

    },

    module = angular.module('starter');

    module.factory('Book', function () {
        return Book;
    });

}(angular));

/* globals angular */
(function (angular) {
    'use strict';

    var ShoppingCartLine = function ShoppingCartLine(jsonData) {

            if (jsonData === undefined || jsonData === null) {
                return;
            }

            this.quantity = jsonData.quantity;

            this.item = jsonData.item;

            this.price = jsonData.price;
        },

        module = angular.module('starter');

    module.factory('ShoppingCartLine', function () {
        return ShoppingCartLine;
    });

}(angular));

/* globals angular */
(function (angular) {

    function Api($http, $httpParamSerializerJQLike, Book) {

        function postLogin(user, callback) {
            var config = {
                'headers': {
                    'content-type': 'application/x-www-form-urlencoded'
                }
            };

            $http.post('http://multimedia.uoc.edu/frontend/auth.php', $httpParamSerializerJQLike(user), config)
                .then(function (response) {
                    callback(response.data);
                });
        }

        function getBooks(page, sucessCallback, errorCallback) {
            var params = {
                params: {
                    page: page !== undefined ? page : 1
                }
            };

            $http.get('http://multimedia.uoc.edu/frontend/getbooks.php', params)
                .then(function (response) {
                    var books = [];

                    if (response.data.status === "KO") {
                        return errorCallback();
                    }

                    angular.forEach(response.data, function(value) {
                        books.push(new Book(value));
                    });

                    sucessCallback(books);
                })
        }

        function getBook(bookId, sucessCallback, errorCallback) {
            var config = {
                'headers': {
                    'content-type': 'application/x-www-form-urlencoded'
                }
            },
                params = {id: bookId};

            $http.post('http://multimedia.uoc.edu/frontend/bookdetail.php', $httpParamSerializerJQLike(params), config)
                .then(function (response) {
                    if (response.data.status === "KO") {
                        return errorCallback();
                    }

                    sucessCallback(new Book(response.data));
                })
        }

        return {
            postLogin: postLogin,
            getBooks: getBooks,
            getBook: getBook
        };
    }

    angular.module('starter').service('Api', ['$http', '$httpParamSerializerJQLike', 'Book', Api]);

}(angular));
/* globals angular */
(function (angular) {

    /**
     * @param {Book} Book
     * @param {ShoppingCartLine} ShoppingCartLine
     * @returns ShoppingCart
     * @constructor
     */
    function ShoppingCart(Book, ShoppingCartLine) {

        /**
         * Ads item to localStorage or updates if exists
         *
         * @param {Book} item
         * @param {int} quantity
         */
        function add(item, quantity) {
            var itemSaved = getLine(item.ISBN),
                items = JSON.parse(window.localStorage.getItem('shopping-cart')) || {} ;

            if (itemSaved !== null && itemSaved.quantity > 0) {
                quantity = parseInt(quantity) + parseInt(itemSaved.quantity);
            }

            items[item.ISBN] = {
                quantity: parseInt(quantity),
                item: JSON.stringify(item)
            };

            window.localStorage.setItem('shopping-cart', JSON.stringify(items));
        }

        /**
         * Total line price (product price * quantity)
         *
         * @param {string} ISBN
         * @returns {number}
         */
        function getLinePrice(ISBN) {
            var line, item;

            if (!exists(ISBN)) {
                return 0;
            }

            line = getLine(ISBN);
            item = JSON.parse(line.item);

            return parseInt(line.quantity) * parseInt(item.price);
        }

        /**
         * Returns cart price (all lines)
         *
         * @returns {number}
         */
        function getTotalPrice() {
            var lines = getLines(),
                total = 0;

            angular.forEach(lines, function (line) {
                total += parseInt(line.price);
            });

            return total;
        }

        /**
         * Checks if book is in cart
         * @param {string} ISBN
         * @returns {boolean}
         */
        function exists(ISBN) {
            var items = JSON.parse(window.localStorage.getItem('shopping-cart')) || {};
            return items[ISBN] !== undefined;
        }

        /**
         * Returns the line by ISBN code
         *
         * @param {string} ISBN
         * @returns {ShoppingCartLine}
         */
        function getLine(ISBN) {
            var lines = JSON.parse(window.localStorage.getItem('shopping-cart')) || {},
                line = lines[ISBN],
                book;

            if (line === undefined) {
                return null;
            }

            book = new Book(JSON.parse(line.item));

            return new ShoppingCartLine({
                quantity: line.quantity,
                item: book,
                price: parseInt(line.quantity) * parseInt(book.price)
            });


        }

        /**
         * Returns all lines of cart
         *
         * @returns {Array<ShoppingCartLine>}
         */
        function getLines() {
            var items = JSON.parse(window.localStorage.getItem('shopping-cart')) || {},
                lines = [];

            for (var key in items) {
                if (!items.hasOwnProperty(key)) {
                    continue;
                }

                lines.push(getLine(key));
            }

            return lines;
        }

        /**
         * Drops line from cart
         *
         * @param {string} ISBN
         */
        function removeLine(ISBN) {
            var items = JSON.parse(window.localStorage.getItem('shopping-cart')) || {};
            delete items[ISBN];

            window.localStorage.setItem('shopping-cart', JSON.stringify(items));
        }

        /**
         * Adds one item to line
         *
         * @param {string} ISBN
         */
        function addItemToLine(ISBN) {
            var items = JSON.parse(window.localStorage.getItem('shopping-cart')) || {};
            items[ISBN].quantity += 1;

            window.localStorage.setItem('shopping-cart', JSON.stringify(items));
        }

        /**
         * Removes item from line
         *
         * @param {string} ISBN
         */
        function extractItemFromLine(ISBN) {
            var items = JSON.parse(window.localStorage.getItem('shopping-cart')) || {};

            items[ISBN].quantity -= 1;

            if (items[ISBN].quantity <= 0) {
                delete items[ISBN];
            }

            window.localStorage.setItem('shopping-cart', JSON.stringify(items));
        }

        return {
            add: add,
            exists: exists,
            removeLine: removeLine,
            getTotalPrice: getTotalPrice,
            getLinePrice: getLinePrice,
            getLine: getLine,
            getLines: getLines,
            addItemToLine: addItemToLine,
            extractItemFromLine: extractItemFromLine
        };
    }

    angular.module('starter').service('ShoppingCart', ['Book', 'ShoppingCartLine', ShoppingCart]);

}(angular));
/* globals angular */
(function (angular) {

    /**
     * User authentication service
     *
     * @param Api
     * @returns {{login: login, isLogged: isLogged}}
     * @constructor
     */
    function User(Api) {
        var self = this;

        /**
         *
         * @param {object} user
         * @param {callback} callback
         */
        function login(user, callback) {
            self.callback = callback;
            Api.postLogin(user, onLogged);
        }

        function onLogged(response) {
            if (response.status === "OK") {
                storeSession();
            }

            self.callback(response);
        }

        /**
         * Sets user as logged out
         */
        function logout() {
            destroySession();
        }

        /**
         * Checks if user is logged
         * @returns {boolean}
         */
        function isLogged() {
            return Boolean(window.localStorage.getItem('active-session')) === true;
        }

        function storeSession() {
            window.localStorage.setItem('active-session', 1);
        }

        function destroySession() {
            window.localStorage.removeItem('active-session');
        }

        return {
            login: login,
            logout: logout,
            isLogged: isLogged
        };
    }

    angular.module('starter').service('User', ['Api', User]);

}(angular));
/* globals angular */
(function (angular) {
    'use strict';

    var module = angular.module('starter');

    module.controller('MainController', function(User, $scope, $state) {
        var self = this;

        this.checkSession = function checkSession() {
            if (!User.isLogged()) {
                $state.go('login');
            }
        };

        $scope.$on('$ionicView.beforeEnter', function(){
            self.checkSession();
        });

        $scope.$on('$ionicView.loaded', function(){
            self.checkSession();
        });

        $scope.logout = function logout() {
            User.logout();
            self.checkSession();
        };

        $scope.goToCart = function goToCart() {
            $state.go('cart');
        };

        $scope.goToIndex = function goToCart() {
            $state.go('main');
        };

    });
}(angular));
/* globals angular */
(function (angular) {
    'use strict';

    var module = angular.module('starter');

    module.controller(
        'BookDetailController',
        function (
            $scope,
            $stateParams,
            $state,
            User,
            Api,
            ShoppingCart,
            $controller,
            $ionicPopup
        ) {

            var self = this;

            $scope.cartSelectItems = [1,2,3,4,5,6,7,8,9,10];

            $scope.itemsToCart = 0;

            angular.extend(this, $controller('MainController', {$scope: $scope}));

            $scope.book = {};

            this.id = $stateParams.bookId;

            $scope.$on('$ionicView.beforeEnter', function(){
                self.checkSession();
                Api.getBook(self.id, onBookLoaded, onNotBooksReceived)
            });

            $scope.addToCart = function addToCart() {
                if ($scope.itemsToCart === 0) {
                    $ionicPopup.alert({
                        title: 'Oops!',
                        template: 'Please, select quantity'
                    });

                    return;
                }

                ShoppingCart.add($scope.book, $scope.itemsToCart);
                $scope.itemsToCart = 0;
                $ionicPopup.alert({
                    title: 'Shopping cart',
                    template: 'Book added to cart'
                });
            };


            function onBookLoaded(response) {
                self.checkSession();
                $scope.book = response;
            }

            function onNotBooksReceived() {
                console.log('error');
            }
    });
}(angular));
/* globals angular */
(function (angular) {
    'use strict';

    var module = angular.module('starter');

    module.controller('BookListController', function($scope, $controller, $state, User, Api) {
        var self = this;

        angular.extend(this, $controller('MainController', {$scope: $scope}));

        $scope.currentPage = 1;

        $scope.noMoreItems = false;

        $scope.books = [];

        function onBooksLoaded(response) {
            self.checkSession();
            $scope.books = $scope.books.concat(response);
            $scope.currentPage++;
        }

        function onBookLoadError() {
            $scope.noMoreItems = true;
        }

        $scope.loadMoreData = function loadMoreData() {
            Api.getBooks($scope.currentPage, onBooksLoaded, onBookLoadError);
            $scope.$broadcast('scroll.infiniteScrollComplete');
        };

        $scope.showDetail = function showDetail(bookId) {
            $state.go('book-detail', {bookId: bookId})
        };

    });
}(angular));
/* globals angular */
(function (angular) {
    'use strict';

    var module = angular.module('starter');

    module.controller('CartController', function(User, $scope, $state, ShoppingCart, $controller, $ionicPopup) {

        angular.extend(this, $controller('MainController', {$scope: $scope}));

        $scope.cart = [];

        $scope.$on('$ionicView.enter', function(){
            $scope.cart = ShoppingCart.getLines();
        });

        $scope.getTotalPrice = function getTotalPrice() {
            return ShoppingCart.getTotalPrice();
        };

        $scope.removeLine = function removeLine(ISBN) {
            ShoppingCart.removeLine(ISBN);
            $scope.cart = ShoppingCart.getLines();
        };

        $scope.addItemToLine = function addItemToLine(ISBN) {
            ShoppingCart.addItemToLine(ISBN);
            $scope.cart = ShoppingCart.getLines();
        };

        $scope.extractItemFromLine = function extractItemFromLine(ISBN) {
            ShoppingCart.extractItemFromLine(ISBN);
            $scope.cart = ShoppingCart.getLines();
        };

        $scope.cartIsEmpty = function cartIsEmpty() {
            return $scope.cart.length === 0
        };

        $scope.checkOut = function checkOut() {
            $ionicPopup.alert({
                title: 'Thank you!',
                template: 'You sent me $' + $scope.getTotalPrice()
            });
        }

    });
}(angular));
/* globals angular */
(function (angular) {
    'use strict';

    var module = angular.module('starter');

    module.controller('LoginController', function (User, $scope, $state) {
        $scope.user = {};

        $scope.submitted = false;

        $scope.$on('$ionicView.beforeEnter', function(){
            if (User.isLogged()) {
                $state.go('main');
            }
        });

        /**
         * Sends sign in form.
         */
        $scope.signIn = function signIn() {
            $scope.submitted = true;

            User.login($scope.user, onLogged);
        };

        /**
         * @callback
         * @param {{status: string}} response
         */
        function onLogged (response) {
            if (response.status === "OK") {
                $state.go('books');
            }

        }



    })
}(angular));
/* globals angular */
(function (angular) {
    'use strict';

    /**
     * Components for header navigation
     *
     * @constructor
     */
    angular.module("starter").component('headerNav', {

        controllerAs: 'ctrl',

        bindings: {
            leftButtonAction: '<',
            rightButtonAction: '<'
        },

        templateUrl: 'templates/component/header_nav.html'
    });

}(angular));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5qcyIsImJvb2suanMiLCJzaG9wcGluZ19jYXJ0X2xpbmUuanMiLCJhcGkuanMiLCJzaG9vcGluZ19jYXJ0LmpzIiwidXNlci5qcyIsIm1haW4uanMiLCJib29rX2RldGFpbC5qcyIsImJvb2tfbGlzdC5qcyIsImNhcnQuanMiLCJsb2dpbi5qcyIsImhlYWRlcl9uYXYuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNsRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNqQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUN4QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDcEVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDcExBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDL0RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNyQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2hFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDckNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQy9DQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDeENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gKCkge1xuICBhbmd1bGFyLm1vZHVsZSgnc3RhcnRlcicsIFsnaW9uaWMnLCAnbmdNZXNzYWdlcyddKVxuXG4gICAgLnJ1bihmdW5jdGlvbigkaW9uaWNQbGF0Zm9ybSkge1xuICAgICAgJGlvbmljUGxhdGZvcm0ucmVhZHkoZnVuY3Rpb24oKSB7XG4gICAgICAgIC8vIEhpZGUgdGhlIGFjY2Vzc29yeSBiYXIgYnkgZGVmYXVsdCAocmVtb3ZlIHRoaXMgdG8gc2hvdyB0aGUgYWNjZXNzb3J5IGJhciBhYm92ZSB0aGUga2V5Ym9hcmRcbiAgICAgICAgLy8gZm9yIGZvcm0gaW5wdXRzKVxuICAgICAgICBpZiAod2luZG93LmNvcmRvdmEgJiYgd2luZG93LmNvcmRvdmEucGx1Z2lucyAmJiB3aW5kb3cuY29yZG92YS5wbHVnaW5zLktleWJvYXJkKSB7XG4gICAgICAgICAgY29yZG92YS5wbHVnaW5zLktleWJvYXJkLmhpZGVLZXlib2FyZEFjY2Vzc29yeUJhcih0cnVlKTtcbiAgICAgICAgICBjb3Jkb3ZhLnBsdWdpbnMuS2V5Ym9hcmQuZGlzYWJsZVNjcm9sbCh0cnVlKTtcblxuICAgICAgICB9XG4gICAgICAgIGlmICh3aW5kb3cuU3RhdHVzQmFyKSB7XG4gICAgICAgICAgLy8gb3JnLmFwYWNoZS5jb3Jkb3ZhLnN0YXR1c2JhciByZXF1aXJlZFxuXG4gICAgICAgICAgICBTdGF0dXNCYXIuc3R5bGVMaWdodENvbnRlbnQoKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfSlcblxuICAgIC5jb25maWcoZnVuY3Rpb24oJHN0YXRlUHJvdmlkZXIsICR1cmxSb3V0ZXJQcm92aWRlcikge1xuICAgICAgLy8gSW9uaWMgdXNlcyBBbmd1bGFyVUkgUm91dGVyIHdoaWNoIHVzZXMgdGhlIGNvbmNlcHQgb2Ygc3RhdGVzXG4gICAgICAvLyBMZWFybiBtb3JlIGhlcmU6IGh0dHBzOi8vZ2l0aHViLmNvbS9hbmd1bGFyLXVpL3VpLXJvdXRlclxuICAgICAgLy8gU2V0IHVwIHRoZSB2YXJpb3VzIHN0YXRlcyB3aGljaCB0aGUgYXBwIGNhbiBiZSBpbi5cbiAgICAgIC8vIEVhY2ggc3RhdGUncyBjb250cm9sbGVyIGNhbiBiZSBmb3VuZCBpbiBjb250cm9sbGVycy5qc1xuICAgICAgJHN0YXRlUHJvdmlkZXJcblxuICAgICAgLy8gc2V0dXAgYW4gYWJzdHJhY3Qgc3RhdGUgZm9yIHRoZSB0YWJzIGRpcmVjdGl2ZVxuICAgICAgICAgIC5zdGF0ZSgnbWFpbicsIHtcbiAgICAgICAgICAgICAgdXJsOiAnLycsXG4gICAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAndGVtcGxhdGVzL21haW4uaHRtbCcsXG4gICAgICAgICAgICAgIGNvbnRyb2xsZXI6ICdNYWluQ29udHJvbGxlcidcbiAgICAgICAgICB9KVxuXG4gICAgICAgICAgLnN0YXRlKCdsb2dpbicsIHtcbiAgICAgICAgICAgIHVybDogJy9sb2dpbicsXG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogJ3RlbXBsYXRlcy9sb2dpbi5odG1sJyxcbiAgICAgICAgICAgIGNvbnRyb2xsZXI6ICdMb2dpbkNvbnRyb2xsZXInXG4gICAgICAgICAgfSlcblxuICAgICAgICAgIC5zdGF0ZSgnYm9va3MnLCB7XG4gICAgICAgICAgICB1cmw6ICcvYm9va3MnLFxuICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICd0ZW1wbGF0ZXMvYm9vay1saXN0Lmh0bWwnLFxuICAgICAgICAgICAgY29udHJvbGxlcjogJ0Jvb2tMaXN0Q29udHJvbGxlcidcbiAgICAgICAgICB9KVxuXG4gICAgICAgICAgLnN0YXRlKCdib29rLWRldGFpbCcsIHtcbiAgICAgICAgICAgIHVybDogJy9ib29rcy86Ym9va0lkJyxcbiAgICAgICAgICAgIHRlbXBsYXRlVXJsOiAndGVtcGxhdGVzL2Jvb2stZGV0YWlsLmh0bWwnLFxuICAgICAgICAgICAgY29udHJvbGxlcjogJ0Jvb2tEZXRhaWxDb250cm9sbGVyJ1xuICAgICAgICAgIH0pXG5cbiAgICAgICAgICAuc3RhdGUoJ2NhcnQnLCB7XG4gICAgICAgICAgICAgIHVybDogJy9jYXJ0JyxcbiAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6ICd0ZW1wbGF0ZXMvY2FydC5odG1sJyxcbiAgICAgICAgICAgICAgY29udHJvbGxlcjogJ0NhcnRDb250cm9sbGVyJ1xuICAgICAgICAgIH0pXG5cbiAgICAgIDtcblxuICAgICAgLy8gaWYgbm9uZSBvZiB0aGUgYWJvdmUgc3RhdGVzIGFyZSBtYXRjaGVkLCB1c2UgdGhpcyBhcyB0aGUgZmFsbGJhY2tcbiAgICAgICR1cmxSb3V0ZXJQcm92aWRlci5vdGhlcndpc2UoJ2xvZ2luJyk7XG5cbiAgICB9KTtcbn0oKSk7XG5cbiIsIi8qIGdsb2JhbHMgYW5ndWxhciAqL1xuKGZ1bmN0aW9uIChhbmd1bGFyKSB7XG4gICAgJ3VzZSBzdHJpY3QnO1xuXG4gICAgdmFyIEJvb2sgPSBmdW5jdGlvbiBCb29rKGpzb25EYXRhKSB7XG5cbiAgICAgICAgaWYgKGpzb25EYXRhID09PSB1bmRlZmluZWQgfHwganNvbkRhdGEgPT09IG51bGwpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuaWQgPSBqc29uRGF0YS5pZDtcblxuICAgICAgICB0aGlzLnRpdGxlID0ganNvbkRhdGEudGl0bGU7XG5cbiAgICAgICAgdGhpcy5jb3ZlciA9IGpzb25EYXRhLmNvdmVyO1xuXG4gICAgICAgIHRoaXMuYXV0aG9yID0ganNvbkRhdGEuYXV0aG9yO1xuXG4gICAgICAgIHRoaXMucHJpY2UgPSBwYXJzZUludChqc29uRGF0YS5wcmljZSk7XG5cbiAgICAgICAgdGhpcy5yZXZpZXcgPSBqc29uRGF0YS5yZXZpZXc7XG5cbiAgICAgICAgdGhpcy5JU0JOID0ganNvbkRhdGEuSVNCTjtcblxuICAgIH0sXG5cbiAgICBtb2R1bGUgPSBhbmd1bGFyLm1vZHVsZSgnc3RhcnRlcicpO1xuXG4gICAgbW9kdWxlLmZhY3RvcnkoJ0Jvb2snLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiBCb29rO1xuICAgIH0pO1xuXG59KGFuZ3VsYXIpKTtcbiIsIi8qIGdsb2JhbHMgYW5ndWxhciAqL1xuKGZ1bmN0aW9uIChhbmd1bGFyKSB7XG4gICAgJ3VzZSBzdHJpY3QnO1xuXG4gICAgdmFyIFNob3BwaW5nQ2FydExpbmUgPSBmdW5jdGlvbiBTaG9wcGluZ0NhcnRMaW5lKGpzb25EYXRhKSB7XG5cbiAgICAgICAgICAgIGlmIChqc29uRGF0YSA9PT0gdW5kZWZpbmVkIHx8IGpzb25EYXRhID09PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLnF1YW50aXR5ID0ganNvbkRhdGEucXVhbnRpdHk7XG5cbiAgICAgICAgICAgIHRoaXMuaXRlbSA9IGpzb25EYXRhLml0ZW07XG5cbiAgICAgICAgICAgIHRoaXMucHJpY2UgPSBqc29uRGF0YS5wcmljZTtcbiAgICAgICAgfSxcblxuICAgICAgICBtb2R1bGUgPSBhbmd1bGFyLm1vZHVsZSgnc3RhcnRlcicpO1xuXG4gICAgbW9kdWxlLmZhY3RvcnkoJ1Nob3BwaW5nQ2FydExpbmUnLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiBTaG9wcGluZ0NhcnRMaW5lO1xuICAgIH0pO1xuXG59KGFuZ3VsYXIpKTtcbiIsIi8qIGdsb2JhbHMgYW5ndWxhciAqL1xuKGZ1bmN0aW9uIChhbmd1bGFyKSB7XG5cbiAgICBmdW5jdGlvbiBBcGkoJGh0dHAsICRodHRwUGFyYW1TZXJpYWxpemVySlFMaWtlLCBCb29rKSB7XG5cbiAgICAgICAgZnVuY3Rpb24gcG9zdExvZ2luKHVzZXIsIGNhbGxiYWNrKSB7XG4gICAgICAgICAgICB2YXIgY29uZmlnID0ge1xuICAgICAgICAgICAgICAgICdoZWFkZXJzJzoge1xuICAgICAgICAgICAgICAgICAgICAnY29udGVudC10eXBlJzogJ2FwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZCdcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAkaHR0cC5wb3N0KCdodHRwOi8vbXVsdGltZWRpYS51b2MuZWR1L2Zyb250ZW5kL2F1dGgucGhwJywgJGh0dHBQYXJhbVNlcmlhbGl6ZXJKUUxpa2UodXNlciksIGNvbmZpZylcbiAgICAgICAgICAgICAgICAudGhlbihmdW5jdGlvbiAocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2socmVzcG9uc2UuZGF0YSk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBnZXRCb29rcyhwYWdlLCBzdWNlc3NDYWxsYmFjaywgZXJyb3JDYWxsYmFjaykge1xuICAgICAgICAgICAgdmFyIHBhcmFtcyA9IHtcbiAgICAgICAgICAgICAgICBwYXJhbXM6IHtcbiAgICAgICAgICAgICAgICAgICAgcGFnZTogcGFnZSAhPT0gdW5kZWZpbmVkID8gcGFnZSA6IDFcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICAkaHR0cC5nZXQoJ2h0dHA6Ly9tdWx0aW1lZGlhLnVvYy5lZHUvZnJvbnRlbmQvZ2V0Ym9va3MucGhwJywgcGFyYW1zKVxuICAgICAgICAgICAgICAgIC50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgICAgICAgICAgICAgICB2YXIgYm9va3MgPSBbXTtcblxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UuZGF0YS5zdGF0dXMgPT09IFwiS09cIikge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGVycm9yQ2FsbGJhY2soKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgIGFuZ3VsYXIuZm9yRWFjaChyZXNwb25zZS5kYXRhLCBmdW5jdGlvbih2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgYm9va3MucHVzaChuZXcgQm9vayh2YWx1ZSkpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICBzdWNlc3NDYWxsYmFjayhib29rcyk7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIGdldEJvb2soYm9va0lkLCBzdWNlc3NDYWxsYmFjaywgZXJyb3JDYWxsYmFjaykge1xuICAgICAgICAgICAgdmFyIGNvbmZpZyA9IHtcbiAgICAgICAgICAgICAgICAnaGVhZGVycyc6IHtcbiAgICAgICAgICAgICAgICAgICAgJ2NvbnRlbnQtdHlwZSc6ICdhcHBsaWNhdGlvbi94LXd3dy1mb3JtLXVybGVuY29kZWQnXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBwYXJhbXMgPSB7aWQ6IGJvb2tJZH07XG5cbiAgICAgICAgICAgICRodHRwLnBvc3QoJ2h0dHA6Ly9tdWx0aW1lZGlhLnVvYy5lZHUvZnJvbnRlbmQvYm9va2RldGFpbC5waHAnLCAkaHR0cFBhcmFtU2VyaWFsaXplckpRTGlrZShwYXJhbXMpLCBjb25maWcpXG4gICAgICAgICAgICAgICAgLnRoZW4oZnVuY3Rpb24gKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZS5kYXRhLnN0YXR1cyA9PT0gXCJLT1wiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZXJyb3JDYWxsYmFjaygpO1xuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgc3VjZXNzQ2FsbGJhY2sobmV3IEJvb2socmVzcG9uc2UuZGF0YSkpO1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgcG9zdExvZ2luOiBwb3N0TG9naW4sXG4gICAgICAgICAgICBnZXRCb29rczogZ2V0Qm9va3MsXG4gICAgICAgICAgICBnZXRCb29rOiBnZXRCb29rXG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgYW5ndWxhci5tb2R1bGUoJ3N0YXJ0ZXInKS5zZXJ2aWNlKCdBcGknLCBbJyRodHRwJywgJyRodHRwUGFyYW1TZXJpYWxpemVySlFMaWtlJywgJ0Jvb2snLCBBcGldKTtcblxufShhbmd1bGFyKSk7IiwiLyogZ2xvYmFscyBhbmd1bGFyICovXG4oZnVuY3Rpb24gKGFuZ3VsYXIpIHtcblxuICAgIC8qKlxuICAgICAqIEBwYXJhbSB7Qm9va30gQm9va1xuICAgICAqIEBwYXJhbSB7U2hvcHBpbmdDYXJ0TGluZX0gU2hvcHBpbmdDYXJ0TGluZVxuICAgICAqIEByZXR1cm5zIFNob3BwaW5nQ2FydFxuICAgICAqIEBjb25zdHJ1Y3RvclxuICAgICAqL1xuICAgIGZ1bmN0aW9uIFNob3BwaW5nQ2FydChCb29rLCBTaG9wcGluZ0NhcnRMaW5lKSB7XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEFkcyBpdGVtIHRvIGxvY2FsU3RvcmFnZSBvciB1cGRhdGVzIGlmIGV4aXN0c1xuICAgICAgICAgKlxuICAgICAgICAgKiBAcGFyYW0ge0Jvb2t9IGl0ZW1cbiAgICAgICAgICogQHBhcmFtIHtpbnR9IHF1YW50aXR5XG4gICAgICAgICAqL1xuICAgICAgICBmdW5jdGlvbiBhZGQoaXRlbSwgcXVhbnRpdHkpIHtcbiAgICAgICAgICAgIHZhciBpdGVtU2F2ZWQgPSBnZXRMaW5lKGl0ZW0uSVNCTiksXG4gICAgICAgICAgICAgICAgaXRlbXMgPSBKU09OLnBhcnNlKHdpbmRvdy5sb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnc2hvcHBpbmctY2FydCcpKSB8fCB7fSA7XG5cbiAgICAgICAgICAgIGlmIChpdGVtU2F2ZWQgIT09IG51bGwgJiYgaXRlbVNhdmVkLnF1YW50aXR5ID4gMCkge1xuICAgICAgICAgICAgICAgIHF1YW50aXR5ID0gcGFyc2VJbnQocXVhbnRpdHkpICsgcGFyc2VJbnQoaXRlbVNhdmVkLnF1YW50aXR5KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaXRlbXNbaXRlbS5JU0JOXSA9IHtcbiAgICAgICAgICAgICAgICBxdWFudGl0eTogcGFyc2VJbnQocXVhbnRpdHkpLFxuICAgICAgICAgICAgICAgIGl0ZW06IEpTT04uc3RyaW5naWZ5KGl0ZW0pXG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICB3aW5kb3cubG9jYWxTdG9yYWdlLnNldEl0ZW0oJ3Nob3BwaW5nLWNhcnQnLCBKU09OLnN0cmluZ2lmeShpdGVtcykpO1xuICAgICAgICB9XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFRvdGFsIGxpbmUgcHJpY2UgKHByb2R1Y3QgcHJpY2UgKiBxdWFudGl0eSlcbiAgICAgICAgICpcbiAgICAgICAgICogQHBhcmFtIHtzdHJpbmd9IElTQk5cbiAgICAgICAgICogQHJldHVybnMge251bWJlcn1cbiAgICAgICAgICovXG4gICAgICAgIGZ1bmN0aW9uIGdldExpbmVQcmljZShJU0JOKSB7XG4gICAgICAgICAgICB2YXIgbGluZSwgaXRlbTtcblxuICAgICAgICAgICAgaWYgKCFleGlzdHMoSVNCTikpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gMDtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgbGluZSA9IGdldExpbmUoSVNCTik7XG4gICAgICAgICAgICBpdGVtID0gSlNPTi5wYXJzZShsaW5lLml0ZW0pO1xuXG4gICAgICAgICAgICByZXR1cm4gcGFyc2VJbnQobGluZS5xdWFudGl0eSkgKiBwYXJzZUludChpdGVtLnByaWNlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBSZXR1cm5zIGNhcnQgcHJpY2UgKGFsbCBsaW5lcylcbiAgICAgICAgICpcbiAgICAgICAgICogQHJldHVybnMge251bWJlcn1cbiAgICAgICAgICovXG4gICAgICAgIGZ1bmN0aW9uIGdldFRvdGFsUHJpY2UoKSB7XG4gICAgICAgICAgICB2YXIgbGluZXMgPSBnZXRMaW5lcygpLFxuICAgICAgICAgICAgICAgIHRvdGFsID0gMDtcblxuICAgICAgICAgICAgYW5ndWxhci5mb3JFYWNoKGxpbmVzLCBmdW5jdGlvbiAobGluZSkge1xuICAgICAgICAgICAgICAgIHRvdGFsICs9IHBhcnNlSW50KGxpbmUucHJpY2UpO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIHJldHVybiB0b3RhbDtcbiAgICAgICAgfVxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBDaGVja3MgaWYgYm9vayBpcyBpbiBjYXJ0XG4gICAgICAgICAqIEBwYXJhbSB7c3RyaW5nfSBJU0JOXG4gICAgICAgICAqIEByZXR1cm5zIHtib29sZWFufVxuICAgICAgICAgKi9cbiAgICAgICAgZnVuY3Rpb24gZXhpc3RzKElTQk4pIHtcbiAgICAgICAgICAgIHZhciBpdGVtcyA9IEpTT04ucGFyc2Uod2luZG93LmxvY2FsU3RvcmFnZS5nZXRJdGVtKCdzaG9wcGluZy1jYXJ0JykpIHx8IHt9O1xuICAgICAgICAgICAgcmV0dXJuIGl0ZW1zW0lTQk5dICE9PSB1bmRlZmluZWQ7XG4gICAgICAgIH1cblxuICAgICAgICAvKipcbiAgICAgICAgICogUmV0dXJucyB0aGUgbGluZSBieSBJU0JOIGNvZGVcbiAgICAgICAgICpcbiAgICAgICAgICogQHBhcmFtIHtzdHJpbmd9IElTQk5cbiAgICAgICAgICogQHJldHVybnMge1Nob3BwaW5nQ2FydExpbmV9XG4gICAgICAgICAqL1xuICAgICAgICBmdW5jdGlvbiBnZXRMaW5lKElTQk4pIHtcbiAgICAgICAgICAgIHZhciBsaW5lcyA9IEpTT04ucGFyc2Uod2luZG93LmxvY2FsU3RvcmFnZS5nZXRJdGVtKCdzaG9wcGluZy1jYXJ0JykpIHx8IHt9LFxuICAgICAgICAgICAgICAgIGxpbmUgPSBsaW5lc1tJU0JOXSxcbiAgICAgICAgICAgICAgICBib29rO1xuXG4gICAgICAgICAgICBpZiAobGluZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGJvb2sgPSBuZXcgQm9vayhKU09OLnBhcnNlKGxpbmUuaXRlbSkpO1xuXG4gICAgICAgICAgICByZXR1cm4gbmV3IFNob3BwaW5nQ2FydExpbmUoe1xuICAgICAgICAgICAgICAgIHF1YW50aXR5OiBsaW5lLnF1YW50aXR5LFxuICAgICAgICAgICAgICAgIGl0ZW06IGJvb2ssXG4gICAgICAgICAgICAgICAgcHJpY2U6IHBhcnNlSW50KGxpbmUucXVhbnRpdHkpICogcGFyc2VJbnQoYm9vay5wcmljZSlcbiAgICAgICAgICAgIH0pO1xuXG5cbiAgICAgICAgfVxuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBSZXR1cm5zIGFsbCBsaW5lcyBvZiBjYXJ0XG4gICAgICAgICAqXG4gICAgICAgICAqIEByZXR1cm5zIHtBcnJheTxTaG9wcGluZ0NhcnRMaW5lPn1cbiAgICAgICAgICovXG4gICAgICAgIGZ1bmN0aW9uIGdldExpbmVzKCkge1xuICAgICAgICAgICAgdmFyIGl0ZW1zID0gSlNPTi5wYXJzZSh3aW5kb3cubG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3Nob3BwaW5nLWNhcnQnKSkgfHwge30sXG4gICAgICAgICAgICAgICAgbGluZXMgPSBbXTtcblxuICAgICAgICAgICAgZm9yICh2YXIga2V5IGluIGl0ZW1zKSB7XG4gICAgICAgICAgICAgICAgaWYgKCFpdGVtcy5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGxpbmVzLnB1c2goZ2V0TGluZShrZXkpKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIGxpbmVzO1xuICAgICAgICB9XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIERyb3BzIGxpbmUgZnJvbSBjYXJ0XG4gICAgICAgICAqXG4gICAgICAgICAqIEBwYXJhbSB7c3RyaW5nfSBJU0JOXG4gICAgICAgICAqL1xuICAgICAgICBmdW5jdGlvbiByZW1vdmVMaW5lKElTQk4pIHtcbiAgICAgICAgICAgIHZhciBpdGVtcyA9IEpTT04ucGFyc2Uod2luZG93LmxvY2FsU3RvcmFnZS5nZXRJdGVtKCdzaG9wcGluZy1jYXJ0JykpIHx8IHt9O1xuICAgICAgICAgICAgZGVsZXRlIGl0ZW1zW0lTQk5dO1xuXG4gICAgICAgICAgICB3aW5kb3cubG9jYWxTdG9yYWdlLnNldEl0ZW0oJ3Nob3BwaW5nLWNhcnQnLCBKU09OLnN0cmluZ2lmeShpdGVtcykpO1xuICAgICAgICB9XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEFkZHMgb25lIGl0ZW0gdG8gbGluZVxuICAgICAgICAgKlxuICAgICAgICAgKiBAcGFyYW0ge3N0cmluZ30gSVNCTlxuICAgICAgICAgKi9cbiAgICAgICAgZnVuY3Rpb24gYWRkSXRlbVRvTGluZShJU0JOKSB7XG4gICAgICAgICAgICB2YXIgaXRlbXMgPSBKU09OLnBhcnNlKHdpbmRvdy5sb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnc2hvcHBpbmctY2FydCcpKSB8fCB7fTtcbiAgICAgICAgICAgIGl0ZW1zW0lTQk5dLnF1YW50aXR5ICs9IDE7XG5cbiAgICAgICAgICAgIHdpbmRvdy5sb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnc2hvcHBpbmctY2FydCcsIEpTT04uc3RyaW5naWZ5KGl0ZW1zKSk7XG4gICAgICAgIH1cblxuICAgICAgICAvKipcbiAgICAgICAgICogUmVtb3ZlcyBpdGVtIGZyb20gbGluZVxuICAgICAgICAgKlxuICAgICAgICAgKiBAcGFyYW0ge3N0cmluZ30gSVNCTlxuICAgICAgICAgKi9cbiAgICAgICAgZnVuY3Rpb24gZXh0cmFjdEl0ZW1Gcm9tTGluZShJU0JOKSB7XG4gICAgICAgICAgICB2YXIgaXRlbXMgPSBKU09OLnBhcnNlKHdpbmRvdy5sb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnc2hvcHBpbmctY2FydCcpKSB8fCB7fTtcblxuICAgICAgICAgICAgaXRlbXNbSVNCTl0ucXVhbnRpdHkgLT0gMTtcblxuICAgICAgICAgICAgaWYgKGl0ZW1zW0lTQk5dLnF1YW50aXR5IDw9IDApIHtcbiAgICAgICAgICAgICAgICBkZWxldGUgaXRlbXNbSVNCTl07XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHdpbmRvdy5sb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnc2hvcHBpbmctY2FydCcsIEpTT04uc3RyaW5naWZ5KGl0ZW1zKSk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgYWRkOiBhZGQsXG4gICAgICAgICAgICBleGlzdHM6IGV4aXN0cyxcbiAgICAgICAgICAgIHJlbW92ZUxpbmU6IHJlbW92ZUxpbmUsXG4gICAgICAgICAgICBnZXRUb3RhbFByaWNlOiBnZXRUb3RhbFByaWNlLFxuICAgICAgICAgICAgZ2V0TGluZVByaWNlOiBnZXRMaW5lUHJpY2UsXG4gICAgICAgICAgICBnZXRMaW5lOiBnZXRMaW5lLFxuICAgICAgICAgICAgZ2V0TGluZXM6IGdldExpbmVzLFxuICAgICAgICAgICAgYWRkSXRlbVRvTGluZTogYWRkSXRlbVRvTGluZSxcbiAgICAgICAgICAgIGV4dHJhY3RJdGVtRnJvbUxpbmU6IGV4dHJhY3RJdGVtRnJvbUxpbmVcbiAgICAgICAgfTtcbiAgICB9XG5cbiAgICBhbmd1bGFyLm1vZHVsZSgnc3RhcnRlcicpLnNlcnZpY2UoJ1Nob3BwaW5nQ2FydCcsIFsnQm9vaycsICdTaG9wcGluZ0NhcnRMaW5lJywgU2hvcHBpbmdDYXJ0XSk7XG5cbn0oYW5ndWxhcikpOyIsIi8qIGdsb2JhbHMgYW5ndWxhciAqL1xuKGZ1bmN0aW9uIChhbmd1bGFyKSB7XG5cbiAgICAvKipcbiAgICAgKiBVc2VyIGF1dGhlbnRpY2F0aW9uIHNlcnZpY2VcbiAgICAgKlxuICAgICAqIEBwYXJhbSBBcGlcbiAgICAgKiBAcmV0dXJucyB7e2xvZ2luOiBsb2dpbiwgaXNMb2dnZWQ6IGlzTG9nZ2VkfX1cbiAgICAgKiBAY29uc3RydWN0b3JcbiAgICAgKi9cbiAgICBmdW5jdGlvbiBVc2VyKEFwaSkge1xuICAgICAgICB2YXIgc2VsZiA9IHRoaXM7XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqXG4gICAgICAgICAqIEBwYXJhbSB7b2JqZWN0fSB1c2VyXG4gICAgICAgICAqIEBwYXJhbSB7Y2FsbGJhY2t9IGNhbGxiYWNrXG4gICAgICAgICAqL1xuICAgICAgICBmdW5jdGlvbiBsb2dpbih1c2VyLCBjYWxsYmFjaykge1xuICAgICAgICAgICAgc2VsZi5jYWxsYmFjayA9IGNhbGxiYWNrO1xuICAgICAgICAgICAgQXBpLnBvc3RMb2dpbih1c2VyLCBvbkxvZ2dlZCk7XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBvbkxvZ2dlZChyZXNwb25zZSkge1xuICAgICAgICAgICAgaWYgKHJlc3BvbnNlLnN0YXR1cyA9PT0gXCJPS1wiKSB7XG4gICAgICAgICAgICAgICAgc3RvcmVTZXNzaW9uKCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHNlbGYuY2FsbGJhY2socmVzcG9uc2UpO1xuICAgICAgICB9XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFNldHMgdXNlciBhcyBsb2dnZWQgb3V0XG4gICAgICAgICAqL1xuICAgICAgICBmdW5jdGlvbiBsb2dvdXQoKSB7XG4gICAgICAgICAgICBkZXN0cm95U2Vzc2lvbigpO1xuICAgICAgICB9XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIENoZWNrcyBpZiB1c2VyIGlzIGxvZ2dlZFxuICAgICAgICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAgICAgICAgICovXG4gICAgICAgIGZ1bmN0aW9uIGlzTG9nZ2VkKCkge1xuICAgICAgICAgICAgcmV0dXJuIEJvb2xlYW4od2luZG93LmxvY2FsU3RvcmFnZS5nZXRJdGVtKCdhY3RpdmUtc2Vzc2lvbicpKSA9PT0gdHJ1ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIHN0b3JlU2Vzc2lvbigpIHtcbiAgICAgICAgICAgIHdpbmRvdy5sb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnYWN0aXZlLXNlc3Npb24nLCAxKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIGRlc3Ryb3lTZXNzaW9uKCkge1xuICAgICAgICAgICAgd2luZG93LmxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKCdhY3RpdmUtc2Vzc2lvbicpO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGxvZ2luOiBsb2dpbixcbiAgICAgICAgICAgIGxvZ291dDogbG9nb3V0LFxuICAgICAgICAgICAgaXNMb2dnZWQ6IGlzTG9nZ2VkXG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgYW5ndWxhci5tb2R1bGUoJ3N0YXJ0ZXInKS5zZXJ2aWNlKCdVc2VyJywgWydBcGknLCBVc2VyXSk7XG5cbn0oYW5ndWxhcikpOyIsIi8qIGdsb2JhbHMgYW5ndWxhciAqL1xuKGZ1bmN0aW9uIChhbmd1bGFyKSB7XG4gICAgJ3VzZSBzdHJpY3QnO1xuXG4gICAgdmFyIG1vZHVsZSA9IGFuZ3VsYXIubW9kdWxlKCdzdGFydGVyJyk7XG5cbiAgICBtb2R1bGUuY29udHJvbGxlcignTWFpbkNvbnRyb2xsZXInLCBmdW5jdGlvbihVc2VyLCAkc2NvcGUsICRzdGF0ZSkge1xuICAgICAgICB2YXIgc2VsZiA9IHRoaXM7XG5cbiAgICAgICAgdGhpcy5jaGVja1Nlc3Npb24gPSBmdW5jdGlvbiBjaGVja1Nlc3Npb24oKSB7XG4gICAgICAgICAgICBpZiAoIVVzZXIuaXNMb2dnZWQoKSkge1xuICAgICAgICAgICAgICAgICRzdGF0ZS5nbygnbG9naW4nKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUuJG9uKCckaW9uaWNWaWV3LmJlZm9yZUVudGVyJywgZnVuY3Rpb24oKXtcbiAgICAgICAgICAgIHNlbGYuY2hlY2tTZXNzaW9uKCk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgICRzY29wZS4kb24oJyRpb25pY1ZpZXcubG9hZGVkJywgZnVuY3Rpb24oKXtcbiAgICAgICAgICAgIHNlbGYuY2hlY2tTZXNzaW9uKCk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgICRzY29wZS5sb2dvdXQgPSBmdW5jdGlvbiBsb2dvdXQoKSB7XG4gICAgICAgICAgICBVc2VyLmxvZ291dCgpO1xuICAgICAgICAgICAgc2VsZi5jaGVja1Nlc3Npb24oKTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUuZ29Ub0NhcnQgPSBmdW5jdGlvbiBnb1RvQ2FydCgpIHtcbiAgICAgICAgICAgICRzdGF0ZS5nbygnY2FydCcpO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5nb1RvSW5kZXggPSBmdW5jdGlvbiBnb1RvQ2FydCgpIHtcbiAgICAgICAgICAgICRzdGF0ZS5nbygnbWFpbicpO1xuICAgICAgICB9O1xuXG4gICAgfSk7XG59KGFuZ3VsYXIpKTsiLCIvKiBnbG9iYWxzIGFuZ3VsYXIgKi9cbihmdW5jdGlvbiAoYW5ndWxhcikge1xuICAgICd1c2Ugc3RyaWN0JztcblxuICAgIHZhciBtb2R1bGUgPSBhbmd1bGFyLm1vZHVsZSgnc3RhcnRlcicpO1xuXG4gICAgbW9kdWxlLmNvbnRyb2xsZXIoXG4gICAgICAgICdCb29rRGV0YWlsQ29udHJvbGxlcicsXG4gICAgICAgIGZ1bmN0aW9uIChcbiAgICAgICAgICAgICRzY29wZSxcbiAgICAgICAgICAgICRzdGF0ZVBhcmFtcyxcbiAgICAgICAgICAgICRzdGF0ZSxcbiAgICAgICAgICAgIFVzZXIsXG4gICAgICAgICAgICBBcGksXG4gICAgICAgICAgICBTaG9wcGluZ0NhcnQsXG4gICAgICAgICAgICAkY29udHJvbGxlcixcbiAgICAgICAgICAgICRpb25pY1BvcHVwXG4gICAgICAgICkge1xuXG4gICAgICAgICAgICB2YXIgc2VsZiA9IHRoaXM7XG5cbiAgICAgICAgICAgICRzY29wZS5jYXJ0U2VsZWN0SXRlbXMgPSBbMSwyLDMsNCw1LDYsNyw4LDksMTBdO1xuXG4gICAgICAgICAgICAkc2NvcGUuaXRlbXNUb0NhcnQgPSAwO1xuXG4gICAgICAgICAgICBhbmd1bGFyLmV4dGVuZCh0aGlzLCAkY29udHJvbGxlcignTWFpbkNvbnRyb2xsZXInLCB7JHNjb3BlOiAkc2NvcGV9KSk7XG5cbiAgICAgICAgICAgICRzY29wZS5ib29rID0ge307XG5cbiAgICAgICAgICAgIHRoaXMuaWQgPSAkc3RhdGVQYXJhbXMuYm9va0lkO1xuXG4gICAgICAgICAgICAkc2NvcGUuJG9uKCckaW9uaWNWaWV3LmJlZm9yZUVudGVyJywgZnVuY3Rpb24oKXtcbiAgICAgICAgICAgICAgICBzZWxmLmNoZWNrU2Vzc2lvbigpO1xuICAgICAgICAgICAgICAgIEFwaS5nZXRCb29rKHNlbGYuaWQsIG9uQm9va0xvYWRlZCwgb25Ob3RCb29rc1JlY2VpdmVkKVxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICRzY29wZS5hZGRUb0NhcnQgPSBmdW5jdGlvbiBhZGRUb0NhcnQoKSB7XG4gICAgICAgICAgICAgICAgaWYgKCRzY29wZS5pdGVtc1RvQ2FydCA9PT0gMCkge1xuICAgICAgICAgICAgICAgICAgICAkaW9uaWNQb3B1cC5hbGVydCh7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ09vcHMhJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIHRlbXBsYXRlOiAnUGxlYXNlLCBzZWxlY3QgcXVhbnRpdHknXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICBTaG9wcGluZ0NhcnQuYWRkKCRzY29wZS5ib29rLCAkc2NvcGUuaXRlbXNUb0NhcnQpO1xuICAgICAgICAgICAgICAgICRzY29wZS5pdGVtc1RvQ2FydCA9IDA7XG4gICAgICAgICAgICAgICAgJGlvbmljUG9wdXAuYWxlcnQoe1xuICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ1Nob3BwaW5nIGNhcnQnLFxuICAgICAgICAgICAgICAgICAgICB0ZW1wbGF0ZTogJ0Jvb2sgYWRkZWQgdG8gY2FydCdcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH07XG5cblxuICAgICAgICAgICAgZnVuY3Rpb24gb25Cb29rTG9hZGVkKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICAgICAgc2VsZi5jaGVja1Nlc3Npb24oKTtcbiAgICAgICAgICAgICAgICAkc2NvcGUuYm9vayA9IHJlc3BvbnNlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBmdW5jdGlvbiBvbk5vdEJvb2tzUmVjZWl2ZWQoKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coJ2Vycm9yJyk7XG4gICAgICAgICAgICB9XG4gICAgfSk7XG59KGFuZ3VsYXIpKTsiLCIvKiBnbG9iYWxzIGFuZ3VsYXIgKi9cbihmdW5jdGlvbiAoYW5ndWxhcikge1xuICAgICd1c2Ugc3RyaWN0JztcblxuICAgIHZhciBtb2R1bGUgPSBhbmd1bGFyLm1vZHVsZSgnc3RhcnRlcicpO1xuXG4gICAgbW9kdWxlLmNvbnRyb2xsZXIoJ0Jvb2tMaXN0Q29udHJvbGxlcicsIGZ1bmN0aW9uKCRzY29wZSwgJGNvbnRyb2xsZXIsICRzdGF0ZSwgVXNlciwgQXBpKSB7XG4gICAgICAgIHZhciBzZWxmID0gdGhpcztcblxuICAgICAgICBhbmd1bGFyLmV4dGVuZCh0aGlzLCAkY29udHJvbGxlcignTWFpbkNvbnRyb2xsZXInLCB7JHNjb3BlOiAkc2NvcGV9KSk7XG5cbiAgICAgICAgJHNjb3BlLmN1cnJlbnRQYWdlID0gMTtcblxuICAgICAgICAkc2NvcGUubm9Nb3JlSXRlbXMgPSBmYWxzZTtcblxuICAgICAgICAkc2NvcGUuYm9va3MgPSBbXTtcblxuICAgICAgICBmdW5jdGlvbiBvbkJvb2tzTG9hZGVkKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICBzZWxmLmNoZWNrU2Vzc2lvbigpO1xuICAgICAgICAgICAgJHNjb3BlLmJvb2tzID0gJHNjb3BlLmJvb2tzLmNvbmNhdChyZXNwb25zZSk7XG4gICAgICAgICAgICAkc2NvcGUuY3VycmVudFBhZ2UrKztcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIG9uQm9va0xvYWRFcnJvcigpIHtcbiAgICAgICAgICAgICRzY29wZS5ub01vcmVJdGVtcyA9IHRydWU7XG4gICAgICAgIH1cblxuICAgICAgICAkc2NvcGUubG9hZE1vcmVEYXRhID0gZnVuY3Rpb24gbG9hZE1vcmVEYXRhKCkge1xuICAgICAgICAgICAgQXBpLmdldEJvb2tzKCRzY29wZS5jdXJyZW50UGFnZSwgb25Cb29rc0xvYWRlZCwgb25Cb29rTG9hZEVycm9yKTtcbiAgICAgICAgICAgICRzY29wZS4kYnJvYWRjYXN0KCdzY3JvbGwuaW5maW5pdGVTY3JvbGxDb21wbGV0ZScpO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5zaG93RGV0YWlsID0gZnVuY3Rpb24gc2hvd0RldGFpbChib29rSWQpIHtcbiAgICAgICAgICAgICRzdGF0ZS5nbygnYm9vay1kZXRhaWwnLCB7Ym9va0lkOiBib29rSWR9KVxuICAgICAgICB9O1xuXG4gICAgfSk7XG59KGFuZ3VsYXIpKTsiLCIvKiBnbG9iYWxzIGFuZ3VsYXIgKi9cbihmdW5jdGlvbiAoYW5ndWxhcikge1xuICAgICd1c2Ugc3RyaWN0JztcblxuICAgIHZhciBtb2R1bGUgPSBhbmd1bGFyLm1vZHVsZSgnc3RhcnRlcicpO1xuXG4gICAgbW9kdWxlLmNvbnRyb2xsZXIoJ0NhcnRDb250cm9sbGVyJywgZnVuY3Rpb24oVXNlciwgJHNjb3BlLCAkc3RhdGUsIFNob3BwaW5nQ2FydCwgJGNvbnRyb2xsZXIsICRpb25pY1BvcHVwKSB7XG5cbiAgICAgICAgYW5ndWxhci5leHRlbmQodGhpcywgJGNvbnRyb2xsZXIoJ01haW5Db250cm9sbGVyJywgeyRzY29wZTogJHNjb3BlfSkpO1xuXG4gICAgICAgICRzY29wZS5jYXJ0ID0gW107XG5cbiAgICAgICAgJHNjb3BlLiRvbignJGlvbmljVmlldy5lbnRlcicsIGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAkc2NvcGUuY2FydCA9IFNob3BwaW5nQ2FydC5nZXRMaW5lcygpO1xuICAgICAgICB9KTtcblxuICAgICAgICAkc2NvcGUuZ2V0VG90YWxQcmljZSA9IGZ1bmN0aW9uIGdldFRvdGFsUHJpY2UoKSB7XG4gICAgICAgICAgICByZXR1cm4gU2hvcHBpbmdDYXJ0LmdldFRvdGFsUHJpY2UoKTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUucmVtb3ZlTGluZSA9IGZ1bmN0aW9uIHJlbW92ZUxpbmUoSVNCTikge1xuICAgICAgICAgICAgU2hvcHBpbmdDYXJ0LnJlbW92ZUxpbmUoSVNCTik7XG4gICAgICAgICAgICAkc2NvcGUuY2FydCA9IFNob3BwaW5nQ2FydC5nZXRMaW5lcygpO1xuICAgICAgICB9O1xuXG4gICAgICAgICRzY29wZS5hZGRJdGVtVG9MaW5lID0gZnVuY3Rpb24gYWRkSXRlbVRvTGluZShJU0JOKSB7XG4gICAgICAgICAgICBTaG9wcGluZ0NhcnQuYWRkSXRlbVRvTGluZShJU0JOKTtcbiAgICAgICAgICAgICRzY29wZS5jYXJ0ID0gU2hvcHBpbmdDYXJ0LmdldExpbmVzKCk7XG4gICAgICAgIH07XG5cbiAgICAgICAgJHNjb3BlLmV4dHJhY3RJdGVtRnJvbUxpbmUgPSBmdW5jdGlvbiBleHRyYWN0SXRlbUZyb21MaW5lKElTQk4pIHtcbiAgICAgICAgICAgIFNob3BwaW5nQ2FydC5leHRyYWN0SXRlbUZyb21MaW5lKElTQk4pO1xuICAgICAgICAgICAgJHNjb3BlLmNhcnQgPSBTaG9wcGluZ0NhcnQuZ2V0TGluZXMoKTtcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUuY2FydElzRW1wdHkgPSBmdW5jdGlvbiBjYXJ0SXNFbXB0eSgpIHtcbiAgICAgICAgICAgIHJldHVybiAkc2NvcGUuY2FydC5sZW5ndGggPT09IDBcbiAgICAgICAgfTtcblxuICAgICAgICAkc2NvcGUuY2hlY2tPdXQgPSBmdW5jdGlvbiBjaGVja091dCgpIHtcbiAgICAgICAgICAgICRpb25pY1BvcHVwLmFsZXJ0KHtcbiAgICAgICAgICAgICAgICB0aXRsZTogJ1RoYW5rIHlvdSEnLFxuICAgICAgICAgICAgICAgIHRlbXBsYXRlOiAnWW91IHNlbnQgbWUgJCcgKyAkc2NvcGUuZ2V0VG90YWxQcmljZSgpXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgfSk7XG59KGFuZ3VsYXIpKTsiLCIvKiBnbG9iYWxzIGFuZ3VsYXIgKi9cbihmdW5jdGlvbiAoYW5ndWxhcikge1xuICAgICd1c2Ugc3RyaWN0JztcblxuICAgIHZhciBtb2R1bGUgPSBhbmd1bGFyLm1vZHVsZSgnc3RhcnRlcicpO1xuXG4gICAgbW9kdWxlLmNvbnRyb2xsZXIoJ0xvZ2luQ29udHJvbGxlcicsIGZ1bmN0aW9uIChVc2VyLCAkc2NvcGUsICRzdGF0ZSkge1xuICAgICAgICAkc2NvcGUudXNlciA9IHt9O1xuXG4gICAgICAgICRzY29wZS5zdWJtaXR0ZWQgPSBmYWxzZTtcblxuICAgICAgICAkc2NvcGUuJG9uKCckaW9uaWNWaWV3LmJlZm9yZUVudGVyJywgZnVuY3Rpb24oKXtcbiAgICAgICAgICAgIGlmIChVc2VyLmlzTG9nZ2VkKCkpIHtcbiAgICAgICAgICAgICAgICAkc3RhdGUuZ28oJ21haW4nKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgLyoqXG4gICAgICAgICAqIFNlbmRzIHNpZ24gaW4gZm9ybS5cbiAgICAgICAgICovXG4gICAgICAgICRzY29wZS5zaWduSW4gPSBmdW5jdGlvbiBzaWduSW4oKSB7XG4gICAgICAgICAgICAkc2NvcGUuc3VibWl0dGVkID0gdHJ1ZTtcblxuICAgICAgICAgICAgVXNlci5sb2dpbigkc2NvcGUudXNlciwgb25Mb2dnZWQpO1xuICAgICAgICB9O1xuXG4gICAgICAgIC8qKlxuICAgICAgICAgKiBAY2FsbGJhY2tcbiAgICAgICAgICogQHBhcmFtIHt7c3RhdHVzOiBzdHJpbmd9fSByZXNwb25zZVxuICAgICAgICAgKi9cbiAgICAgICAgZnVuY3Rpb24gb25Mb2dnZWQgKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICBpZiAocmVzcG9uc2Uuc3RhdHVzID09PSBcIk9LXCIpIHtcbiAgICAgICAgICAgICAgICAkc3RhdGUuZ28oJ2Jvb2tzJyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgfVxuXG5cblxuICAgIH0pXG59KGFuZ3VsYXIpKTsiLCIvKiBnbG9iYWxzIGFuZ3VsYXIgKi9cbihmdW5jdGlvbiAoYW5ndWxhcikge1xuICAgICd1c2Ugc3RyaWN0JztcblxuICAgIC8qKlxuICAgICAqIENvbXBvbmVudHMgZm9yIGhlYWRlciBuYXZpZ2F0aW9uXG4gICAgICpcbiAgICAgKiBAY29uc3RydWN0b3JcbiAgICAgKi9cbiAgICBhbmd1bGFyLm1vZHVsZShcInN0YXJ0ZXJcIikuY29tcG9uZW50KCdoZWFkZXJOYXYnLCB7XG5cbiAgICAgICAgY29udHJvbGxlckFzOiAnY3RybCcsXG5cbiAgICAgICAgYmluZGluZ3M6IHtcbiAgICAgICAgICAgIGxlZnRCdXR0b25BY3Rpb246ICc8JyxcbiAgICAgICAgICAgIHJpZ2h0QnV0dG9uQWN0aW9uOiAnPCdcbiAgICAgICAgfSxcblxuICAgICAgICB0ZW1wbGF0ZVVybDogJ3RlbXBsYXRlcy9jb21wb25lbnQvaGVhZGVyX25hdi5odG1sJ1xuICAgIH0pO1xuXG59KGFuZ3VsYXIpKTsiXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
