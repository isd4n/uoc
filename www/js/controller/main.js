/* globals angular */
(function (angular) {
    'use strict';

    var module = angular.module('starter');

    module.controller('MainController', function(User, $scope, $state) {
        var self = this;

        this.checkSession = function checkSession() {
            if (!User.isLogged()) {
                $state.go('login');
            }
        };

        $scope.$on('$ionicView.beforeEnter', function(){
            self.checkSession();
        });

        $scope.$on('$ionicView.loaded', function(){
            self.checkSession();
        });

        $scope.logout = function logout() {
            User.logout();
            self.checkSession();
        };

        $scope.goToCart = function goToCart() {
            $state.go('cart');
        };

        $scope.goToIndex = function goToCart() {
            $state.go('main');
        };

    });
}(angular));