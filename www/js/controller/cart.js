/* globals angular */
(function (angular) {
    'use strict';

    var module = angular.module('starter');

    module.controller('CartController', function(User, $scope, $state, ShoppingCart, $controller, $ionicPopup) {

        angular.extend(this, $controller('MainController', {$scope: $scope}));

        $scope.cart = [];

        $scope.$on('$ionicView.enter', function(){
            $scope.cart = ShoppingCart.getLines();
        });

        $scope.getTotalPrice = function getTotalPrice() {
            return ShoppingCart.getTotalPrice();
        };

        $scope.removeLine = function removeLine(ISBN) {
            ShoppingCart.removeLine(ISBN);
            $scope.cart = ShoppingCart.getLines();
        };

        $scope.addItemToLine = function addItemToLine(ISBN) {
            ShoppingCart.addItemToLine(ISBN);
            $scope.cart = ShoppingCart.getLines();
        };

        $scope.extractItemFromLine = function extractItemFromLine(ISBN) {
            ShoppingCart.extractItemFromLine(ISBN);
            $scope.cart = ShoppingCart.getLines();
        };

        $scope.cartIsEmpty = function cartIsEmpty() {
            return $scope.cart.length === 0
        };

        $scope.checkOut = function checkOut() {
            $ionicPopup.alert({
                title: 'Thank you!',
                template: 'You sent me $' + $scope.getTotalPrice()
            });
        }

    });
}(angular));