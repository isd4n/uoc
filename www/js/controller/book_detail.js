/* globals angular */
(function (angular) {
    'use strict';

    var module = angular.module('starter');

    module.controller(
        'BookDetailController',
        function (
            $scope,
            $stateParams,
            $state,
            User,
            Api,
            ShoppingCart,
            $controller,
            $ionicPopup
        ) {

            var self = this;

            $scope.cartSelectItems = [1,2,3,4,5,6,7,8,9,10];

            $scope.itemsToCart = 0;

            angular.extend(this, $controller('MainController', {$scope: $scope}));

            $scope.book = {};

            this.id = $stateParams.bookId;

            $scope.$on('$ionicView.beforeEnter', function(){
                self.checkSession();
                Api.getBook(self.id, onBookLoaded, onNotBooksReceived)
            });

            $scope.addToCart = function addToCart() {
                if ($scope.itemsToCart === 0) {
                    $ionicPopup.alert({
                        title: 'Oops!',
                        template: 'Please, select quantity'
                    });

                    return;
                }

                ShoppingCart.add($scope.book, $scope.itemsToCart);
                $scope.itemsToCart = 0;
                $ionicPopup.alert({
                    title: 'Shopping cart',
                    template: 'Book added to cart'
                });
            };


            function onBookLoaded(response) {
                self.checkSession();
                $scope.book = response;
            }

            function onNotBooksReceived() {
                console.log('error');
            }
    });
}(angular));