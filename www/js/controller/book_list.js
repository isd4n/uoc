/* globals angular */
(function (angular) {
    'use strict';

    var module = angular.module('starter');

    module.controller('BookListController', function($scope, $controller, $state, User, Api) {
        var self = this;

        angular.extend(this, $controller('MainController', {$scope: $scope}));

        $scope.currentPage = 1;

        $scope.noMoreItems = false;

        $scope.books = [];

        function onBooksLoaded(response) {
            self.checkSession();
            $scope.books = $scope.books.concat(response);
            $scope.currentPage++;
        }

        function onBookLoadError() {
            $scope.noMoreItems = true;
        }

        $scope.loadMoreData = function loadMoreData() {
            Api.getBooks($scope.currentPage, onBooksLoaded, onBookLoadError);
            $scope.$broadcast('scroll.infiniteScrollComplete');
        };

        $scope.showDetail = function showDetail(bookId) {
            $state.go('book-detail', {bookId: bookId})
        };

    });
}(angular));