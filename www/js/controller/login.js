/* globals angular */
(function (angular) {
    'use strict';

    var module = angular.module('starter');

    module.controller('LoginController', function (User, $scope, $state) {
        $scope.user = {};

        $scope.submitted = false;

        $scope.$on('$ionicView.beforeEnter', function(){
            if (User.isLogged()) {
                $state.go('main');
            }
        });

        /**
         * Sends sign in form.
         */
        $scope.signIn = function signIn() {
            $scope.submitted = true;

            User.login($scope.user, onLogged);
        };

        /**
         * @callback
         * @param {{status: string}} response
         */
        function onLogged (response) {
            if (response.status === "OK") {
                $state.go('books');
            }

        }



    })
}(angular));