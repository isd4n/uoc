/* globals angular */
(function (angular) {
    'use strict';

    /**
     * Components for header navigation
     *
     * @constructor
     */
    angular.module("starter").component('headerNav', {

        controllerAs: 'ctrl',

        bindings: {
            leftButtonAction: '<',
            rightButtonAction: '<'
        },

        templateUrl: 'templates/component/header_nav.html'
    });

}(angular));