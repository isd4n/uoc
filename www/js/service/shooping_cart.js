/* globals angular */
(function (angular) {

    /**
     * @param {Book} Book
     * @param {ShoppingCartLine} ShoppingCartLine
     * @returns ShoppingCart
     * @constructor
     */
    function ShoppingCart(Book, ShoppingCartLine) {

        /**
         * Ads item to localStorage or updates if exists
         *
         * @param {Book} item
         * @param {int} quantity
         */
        function add(item, quantity) {
            var itemSaved = getLine(item.ISBN),
                items = JSON.parse(window.localStorage.getItem('shopping-cart')) || {} ;

            if (itemSaved !== null && itemSaved.quantity > 0) {
                quantity = parseInt(quantity) + parseInt(itemSaved.quantity);
            }

            items[item.ISBN] = {
                quantity: parseInt(quantity),
                item: JSON.stringify(item)
            };

            window.localStorage.setItem('shopping-cart', JSON.stringify(items));
        }

        /**
         * Total line price (product price * quantity)
         *
         * @param {string} ISBN
         * @returns {number}
         */
        function getLinePrice(ISBN) {
            var line, item;

            if (!exists(ISBN)) {
                return 0;
            }

            line = getLine(ISBN);
            item = JSON.parse(line.item);

            return parseInt(line.quantity) * parseInt(item.price);
        }

        /**
         * Returns cart price (all lines)
         *
         * @returns {number}
         */
        function getTotalPrice() {
            var lines = getLines(),
                total = 0;

            angular.forEach(lines, function (line) {
                total += parseInt(line.price);
            });

            return total;
        }

        /**
         * Checks if book is in cart
         * @param {string} ISBN
         * @returns {boolean}
         */
        function exists(ISBN) {
            var items = JSON.parse(window.localStorage.getItem('shopping-cart')) || {};
            return items[ISBN] !== undefined;
        }

        /**
         * Returns the line by ISBN code
         *
         * @param {string} ISBN
         * @returns {ShoppingCartLine}
         */
        function getLine(ISBN) {
            var lines = JSON.parse(window.localStorage.getItem('shopping-cart')) || {},
                line = lines[ISBN],
                book;

            if (line === undefined) {
                return null;
            }

            book = new Book(JSON.parse(line.item));

            return new ShoppingCartLine({
                quantity: line.quantity,
                item: book,
                price: parseInt(line.quantity) * parseInt(book.price)
            });


        }

        /**
         * Returns all lines of cart
         *
         * @returns {Array<ShoppingCartLine>}
         */
        function getLines() {
            var items = JSON.parse(window.localStorage.getItem('shopping-cart')) || {},
                lines = [];

            for (var key in items) {
                if (!items.hasOwnProperty(key)) {
                    continue;
                }

                lines.push(getLine(key));
            }

            return lines;
        }

        /**
         * Drops line from cart
         *
         * @param {string} ISBN
         */
        function removeLine(ISBN) {
            var items = JSON.parse(window.localStorage.getItem('shopping-cart')) || {};
            delete items[ISBN];

            window.localStorage.setItem('shopping-cart', JSON.stringify(items));
        }

        /**
         * Adds one item to line
         *
         * @param {string} ISBN
         */
        function addItemToLine(ISBN) {
            var items = JSON.parse(window.localStorage.getItem('shopping-cart')) || {};
            items[ISBN].quantity += 1;

            window.localStorage.setItem('shopping-cart', JSON.stringify(items));
        }

        /**
         * Removes item from line
         *
         * @param {string} ISBN
         */
        function extractItemFromLine(ISBN) {
            var items = JSON.parse(window.localStorage.getItem('shopping-cart')) || {};

            items[ISBN].quantity -= 1;

            if (items[ISBN].quantity <= 0) {
                delete items[ISBN];
            }

            window.localStorage.setItem('shopping-cart', JSON.stringify(items));
        }

        return {
            add: add,
            exists: exists,
            removeLine: removeLine,
            getTotalPrice: getTotalPrice,
            getLinePrice: getLinePrice,
            getLine: getLine,
            getLines: getLines,
            addItemToLine: addItemToLine,
            extractItemFromLine: extractItemFromLine
        };
    }

    angular.module('starter').service('ShoppingCart', ['Book', 'ShoppingCartLine', ShoppingCart]);

}(angular));