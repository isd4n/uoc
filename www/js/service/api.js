/* globals angular */
(function (angular) {

    function Api($http, $httpParamSerializerJQLike, Book) {

        function postLogin(user, callback) {
            var config = {
                'headers': {
                    'content-type': 'application/x-www-form-urlencoded'
                }
            };

            $http.post('http://multimedia.uoc.edu/frontend/auth.php', $httpParamSerializerJQLike(user), config)
                .then(function (response) {
                    callback(response.data);
                });
        }

        function getBooks(page, sucessCallback, errorCallback) {
            var params = {
                params: {
                    page: page !== undefined ? page : 1
                }
            };

            $http.get('http://multimedia.uoc.edu/frontend/getbooks.php', params)
                .then(function (response) {
                    var books = [];

                    if (response.data.status === "KO") {
                        return errorCallback();
                    }

                    angular.forEach(response.data, function(value) {
                        books.push(new Book(value));
                    });

                    sucessCallback(books);
                })
        }

        function getBook(bookId, sucessCallback, errorCallback) {
            var config = {
                'headers': {
                    'content-type': 'application/x-www-form-urlencoded'
                }
            },
                params = {id: bookId};

            $http.post('http://multimedia.uoc.edu/frontend/bookdetail.php', $httpParamSerializerJQLike(params), config)
                .then(function (response) {
                    if (response.data.status === "KO") {
                        return errorCallback();
                    }

                    sucessCallback(new Book(response.data));
                })
        }

        return {
            postLogin: postLogin,
            getBooks: getBooks,
            getBook: getBook
        };
    }

    angular.module('starter').service('Api', ['$http', '$httpParamSerializerJQLike', 'Book', Api]);

}(angular));