/* globals angular */
(function (angular) {

    /**
     * User authentication service
     *
     * @param Api
     * @returns {{login: login, isLogged: isLogged}}
     * @constructor
     */
    function User(Api) {
        var self = this;

        /**
         *
         * @param {object} user
         * @param {callback} callback
         */
        function login(user, callback) {
            self.callback = callback;
            Api.postLogin(user, onLogged);
        }

        function onLogged(response) {
            if (response.status === "OK") {
                storeSession();
            }

            self.callback(response);
        }

        /**
         * Sets user as logged out
         */
        function logout() {
            destroySession();
        }

        /**
         * Checks if user is logged
         * @returns {boolean}
         */
        function isLogged() {
            return Boolean(window.localStorage.getItem('active-session')) === true;
        }

        function storeSession() {
            window.localStorage.setItem('active-session', 1);
        }

        function destroySession() {
            window.localStorage.removeItem('active-session');
        }

        return {
            login: login,
            logout: logout,
            isLogged: isLogged
        };
    }

    angular.module('starter').service('User', ['Api', User]);

}(angular));